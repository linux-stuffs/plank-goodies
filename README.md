# Plank Goodies

A collections of utils and default settings for your Plank dock.

## INSTALLATION

### Install from source:
Unpack the source package and run command (like root):

```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build Debian package
You need these packages for building the Debian package:
```
build-essential make fakeroot util-linux debianutils lintian dpkg coreutils
```

Run command (like root):
```
make build-deb
```

### Install from *.deb package:
```
dpkg -i plank-goodies*.deb
```

### Build ArchLinux package
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:
Run command (like root):
```
pacman -U plank-goodies-*.pkg.tar.xz
```
