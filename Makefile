SHELL=/bin/bash

DNAME = plank-goodies
VERSION = "0.0.1"
ARCH = "all"

PTHDIR = /usr/share
RM = /bin/rm
CP = /bin/cp
TAR = /bin/tar
MKDIR = /bin/mkdir

install:
	install -d $(DESTDIR)/etc/skel/.config/plank/
	install -d $(DESTDIR)/$(PTHDIR)/doc/$(DNAME)
	install -d $(DESTDIR)/usr/bin
	${CP} -r --no-preserve=ownership $(DNAME)/usr/bin/* $(DESTDIR)/usr/bin/
	${CP} -r --no-preserve=ownership $(DNAME)/etc/skel/.config/plank/* $(DESTDIR)/etc/skel/.config/plank/
	${CP} --no-preserve=ownership $(DNAME)/$(PTHDIR)/doc/$(DNAME)/*  $(DESTDIR)/$(PTHDIR)/doc/$(DNAME)/

uninstall:
	${RM} -rf $(DESTDIR)/etc/skel/.config/plank
	${RM} -rf $(DESTDIR)/usr/bin/plank-reset-default
	${RM} -rf $(DESTDIR)/$(PTHDIR)/doc/$(DNAME)

build-deb:
	make clean
	./build-deb.sh ${DNAME} ${VERSION} ${ARCH}

build-arch:
	make clean
	makepkg --noextract

clean:
	${RM} -f *.deb
	${RM} -f *.pkg.tar.xz
	${RM} -rf pkg/
	${RM} -rf src/
